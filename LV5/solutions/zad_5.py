import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt

image = mpimg.imread('../resources/example.png') 

X = image.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
image_compressed=values[labels]
image_compressed.shape = image.shape
plt.figure(1)
plt.imshow(image)
plt.figure(2)
plt.imshow(image_compressed)
plt.show()
