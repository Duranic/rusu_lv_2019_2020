import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt

image = mpimg.imread('../resources/example_grayscale.png') 

X = image.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=35,n_init=1)
k_means.fit(X)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
image_compressed=values[labels]
image_compressed.shape = image.shape
plt.figure(1)
plt.imshow(image, cmap='gray')
plt.figure(2)
plt.imshow(image_compressed, cmap='gray')
plt.show()

# ako se koristi 10 klastera za svjetlinu svakog piksela biti ce potrebno
# 4 bita jer je 2^4=16>10. ako je slika bila skladistena koristeci 8 bita
# za svaki piksel (vrijednosti 0-255) onda smo postigli 50% kompresiju