# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 16:26:32 2021

@author: student
"""

from sklearn import datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np


def generate_data(n_samples, flagc):

    if (flagc == 1):
        random_state = 365
        X,Y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)

    elif (flagc == 2):
        random_state = 148
        X,Y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    elif flagc == 3:
        random_state = 148
        X, Y = datasets.make_blobs(n_samples=n_samples, cluster_std=[1.0, 2.5, 0.5],
                          random_state=random_state)
    elif flagc == 4:
        X, Y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

    elif flagc == 5:
        X, Y = datasets.make_moons(n_samples=n_samples, noise=.05)

    else:
        X = []

    return X


x=generate_data(500, 1)
kriterij=[]
for i in range(1, 20):
    kmeans=KMeans(n_clusters=i).fit(x)
    kriterij.append(kmeans.inertia_)
    
plt.plot(range(1, 20),kriterij)
plt.xticks(range(1, 20))
plt.show()
# optimalan broj klastera bi se odredio pojavom koljena na grafu, trenutak kada se vrijednost kriterijske funkcije prestane naglo smanjivati
