import numpy as np
import matplotlib.pyplot as plt

randomMaleHeight=np.random.normal(180,7,100)
randomFemaleHeight=np.random.normal(167,7,100)

x=np.random.randint(2, size=100)
heights=np.copy(x)
mask=x.astype(bool)
heights[mask]=randomMaleHeight[mask]
heights[~mask]=randomFemaleHeight[~mask]

heightVector=np.reshape(heights, (100,1))
xVector=np.reshape(x, (1,100))
maleSum=np.dot(xVector, heightVector)
femaleSum=np.dot(1-xVector, heightVector)
maleAvg=maleSum/np.sum(x)
femaleAvg=femaleSum/np.sum(1-x)

male=plt.scatter(x[mask], heights[mask], color="blue")
female=plt.scatter(x[~mask], heights[~mask], color="red")
maleAvg=plt.scatter(1, maleAvg, color="blue",marker="x",s=400)
femaleAvg=plt.scatter(0, femaleAvg, color="red",marker="x",s=400)
plt.ylabel('visina [cm]')
plt.legend((male, maleAvg, female, femaleAvg), ('muške osobe', 'prosječna muška visina', 'ženske osobe', 'prosječna ženska visina'))
plt.xlim(-1, 2)
ticks=[0,1]
plt.xticks(ticks)
plt.show()