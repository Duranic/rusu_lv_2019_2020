import numpy as np
import matplotlib.pyplot as plt

kocka = np.random.randint(1, 7, size=100)
plt.hist(kocka, bins=16)
plt.show()