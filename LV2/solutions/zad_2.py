import re

try:
    fhand = open("mbox-short.txt")
except:
    print ('File cannot be opened')
    exit()
file=fhand.read()
# 1. sadrži najmanje jedno slovo a 
emails=re.findall("(\S*a+\S*)@\S*\.\S*", file)
print(emails)
print()

# 2.  sadrži točno jedno slovo a
emails=re.findall("(\S*a\S*)@\S*\.\S*", file)
print(emails)
print()

# 4. sadrži jedan ili više numeričkih znakova (0 – 9) 

emails=re.findall("(\S*[0–9]+\S*)@\S*\.\S*", file)
print(emails)
print()

# 5. sadrži samo mala slova (a-z) 

emails=re.findall("(\S*[a–z]+\S*)@\S*\.\S*", file)
print(emails)
print()
