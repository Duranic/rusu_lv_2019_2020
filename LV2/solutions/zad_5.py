import numpy as np
import matplotlib.pyplot as plt

try:
    fhand = open("../resources/mtcars.csv")
except:
    print ('File cannot be opened')
    exit()

cars=[]

#skip the first line
next(fhand)

for line in fhand:
    attributes = line.split(',')
    car={}
    car['name'] = (attributes)[0].replace('"',"")
    car['horsepower'] = int((attributes)[4])
    car['mpg'] = float((attributes)[1])
    car['weight'] = float((attributes)[6])
    cars.append(car)
cars = sorted(cars, key=lambda car: car['horsepower'])

plt.scatter(list(car['horsepower'] for car in cars), list(car['mpg'] for car in cars))

for car in cars:
    plt.annotate(car['weight'], (car['horsepower'],car['mpg']), xytext=(5,-4),textcoords="offset points")

plt.xlabel('Horsepower')
plt.ylabel('Miles per gallon')
plt.show()