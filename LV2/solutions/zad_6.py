import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('../resources/tiger.png')
img[:,:,:]+=0.25
plt.imshow(img)
plt.show()