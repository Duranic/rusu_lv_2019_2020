ocjena=float(input('Unesi ocjenu(0.0-1.0): '))
try:
    if(ocjena>1 or ocjena<0):
        raise ValueError('vrijednost izvan intervala', ocjena)
except ValueError as err:
    print(repr(err))
else:
    if(ocjena>=0.9): print('A')
    elif(ocjena>=0.8): print('B')
    elif(ocjena>=0.7): print('C')
    elif(ocjena>=0.6): print('D')
    else: print('F')
