fname = input('Enter the file name: ')
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
emails=[]
hostnames={}
for line in fhand:
    if line.startswith("From"):
        email=(line.split())[1]
        hostname=(email.split('@'))[1]
        if hostname in hostnames:
            hostnames[hostname]=hostnames.get(hostname)+1
        else:
            hostnames[hostname]=1
        emails.append(email)
print("emails: ", emails[:10])
print()
print("dictionary entries: ", list(hostnames.items())[:10])
