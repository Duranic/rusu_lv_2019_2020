fname = input('Enter the file name: ')
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
confidences=[]
avg=0.0
for line in fhand:
    if "X-DSPAM-Confidence:" in line:
        confidences.append(float(line[20:]))
for confidence in confidences:
    avg+=confidence
avg/=len(confidences)
print(avg)