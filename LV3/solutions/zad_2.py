import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

#bar plot srednjih potrosnji u ovisnosti o broju cilindara
y1=mtcars[mtcars['cyl']==4]['mpg'].mean()
plt.bar(4, y1, width=0.8)
y2=mtcars[mtcars['cyl']==6]['mpg'].mean()
plt.bar(6, y2, width=0.8)
y3=mtcars[mtcars['cyl']==8]['mpg'].mean()
plt.bar(8, y3, width=0.8)
plt.show()

#box plot potrosnji u ovisnosti o broju cilindara
y1=np.array(mtcars[mtcars['cyl']==4]['mpg']).astype(np.float)
y2=np.array(mtcars[mtcars['cyl']==6]['mpg']).astype(np.float)
y3=np.array(mtcars[mtcars['cyl']==8]['mpg']).astype(np.float)
data=[y1, y2, y3]
plt.boxplot(data, positions=[4, 6, 8])
plt.show()

#imaju li automobili s rucnim mjenjacem vecu potrosnju nego automatici?
y1=np.array(mtcars[mtcars['am']==0]['mpg']).astype(np.float)
y2=np.array(mtcars[mtcars['am']==1]['mpg']).astype(np.float)
data=[y1, y2]
plt.boxplot(data, positions=[0, 1])
plt.show()
#automobili s rucnim mjenjacem imaju manju potrosnju

#odnos ubrzanja i snage
x1=np.array(mtcars[mtcars['am']==0][['hp','qsec']].sort_values('hp'))[:,0]
y1=np.array(mtcars[mtcars['am']==0][['hp','qsec']].sort_values('hp'))[:,1]
plt.scatter(x1, y1)
x2=np.array(mtcars[mtcars['am']==1][['hp','qsec']].sort_values('hp'))[:,0]
y2=np.array(mtcars[mtcars['am']==1][['hp','qsec']].sort_values('hp'))[:,1]
plt.scatter(x2, y2)
pravac=x1.copy()
pravac[0]=min(x1[0],x2[0])-50
pravac[pravac.size-1]=max(x1[x1.size-1], x2[x2.size-1])+50
plt.plot(pravac, np.poly1d(np.polyfit(x1, y1, 1))(pravac))
plt.plot(pravac, np.poly1d(np.polyfit(x2, y2, 1))(pravac))
plt.show()