import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

print(mtcars[mtcars.cyl > 6]) 

# 1. Kojih 5 automobila ima najveću potrošnju?
print(mtcars.sort_values('mpg').head(5))

# 2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju? 
print(mtcars.query('cyl == 8').sort_values('mpg').head(3))

# 3. Kolika je srednja potrošnja automobila sa 6 cilindara? 
print(mtcars.query('cyl == 6').mpg.mean())

# 4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs? 
print(mtcars[(mtcars.wt>2) & (mtcars.wt<2.2) & (mtcars.cyl==4)])

# 5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka? 
print("broj automatskih automobila:", mtcars.query('am == 0').am.count())
print("broj manualnih automobila:", mtcars.query('am == 1').am.count())

# 6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga? 
print(mtcars.query('am == 0')[mtcars.hp>180])

# 7. Kolika je masa svakog automobila u kilogramima? 
print((mtcars.wt)*1000/2.2)


