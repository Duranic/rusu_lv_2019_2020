from math import degrees
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn import metrics

np.random.seed(242)

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1))
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1))
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2)
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data


def plot_confusion_matrix(c_matrix):

    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure("6. zadatak")
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])
    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                            horizontalalignment='center',
                            verticalalignment='center',
                            color = 'green', size = 20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

degree=2
data_train = generate_data(200)
data_test = generate_data(100)
poly = PolynomialFeatures(degree=degree, include_bias = False)
data_train_new = poly.fit_transform(data_train[:,:2])
data_test_new = poly.fit_transform(data_test[:,:2])

colors=["red","blue"]
plt.figure("3. zadatak",figsize=(8,6))
ax=plt.gca()
plt.scatter(data_train[:,0], data_train[:,1],c=data_train[:,2],cmap=matplotlib.colors.ListedColormap(colors))
model=LogisticRegression(max_iter=2000)
model.fit(data_train_new, data_train[:,2])

x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
 min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = poly.fit_transform(np.c_[x_grid.ravel(), y_grid.ravel()])
probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)
plt.contour(x_grid, y_grid, probs, 60, levels=[0.5], cmap=matplotlib.colors.ListedColormap("black"))
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')


f=plt.figure("4. zadatak",figsize=(8,6))
ax=plt.gca()
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
ax.scatter(data_train[:,0], data_train[:,1],c=data_train[:,2],cmap=matplotlib.colors.ListedColormap(colors))


f=plt.figure("5. zadatak",figsize=(8,6))
ax=plt.gca()
colors=["black", (0,1,0)]
y=model.predict(data_test_new)
plt.scatter(data_test[:,0], data_test[:,1],c=(data_test[:,2]==y),cmap=matplotlib.colors.ListedColormap(colors),vmin=False, vmax=True)
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
plt.title('Predvideno modelom')

matrix=metrics.confusion_matrix(data_test[:,2], y)
accuracy=metrics.accuracy_score(data_test[:,2], y)
precision=metrics.precision_score(data_test[:,2], y)
recall=metrics.recall_score(data_test[:,2], y)
specificity=metrics.recall_score(data_test[:,2], y, pos_label=0)

print("Accuracy:", accuracy)
print("Precision:", precision)
print("Recall:", recall)
print("Specificity:", specificity)

plot_confusion_matrix(matrix)


