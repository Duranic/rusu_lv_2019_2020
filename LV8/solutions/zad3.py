from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.preprocessing import image_dataset_from_directory


train_ds = image_dataset_from_directory(
    directory='archive/Train/',
    labels='inferred',
    label_mode='categorical',
    batch_size=32,
    image_size=(48, 48))

model = keras.Sequential(
    [
        keras.Input(shape=(48,48,3)),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPool2D(pool_size=(2,2), strides=(2,2)),
        layers.Dropout(0.2),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPool2D(pool_size=(2,2), strides=(2,2)),
        layers.Dropout(0.2),
        layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
        layers.MaxPool2D(pool_size=(2,2), strides=(2,2)),
        layers.Dropout(0.2),
        layers.Flatten(),
        layers.Dense(512, activation="relu"),
        layers.Dropout(0.5),
        layers.Dense(43, activation="softmax"),
    ]
)

model.summary()

model.fit_generator(train_ds, epochs=5)
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

test_ds = image_dataset_from_directory(
    directory='archive/Test_dir/',
    labels='inferred',
    label_mode='categorical',
    batch_size=32,
    image_size=(48, 48))

eval = model.evaluate_generator(test_ds)
print(eval)