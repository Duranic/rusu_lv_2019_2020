import os
import shutil
import pandas as pd

test_csv=pd.read_csv('archive\Test.csv')
os.mkdir("archive\Test_dir")
test_dir_path="archive\Test_dir"
for i in range(0,43):
    dir=str(i)
    path=os.path.join(test_dir_path, dir)
    print(path)
    os.mkdir(path)

for i in range (0,len(test_csv)):
    image_path=str(test_csv["Path"][i])
    image_path=os.path.join("archive", image_path)
    folder=str(test_csv["ClassId"][i])
    folder_path=os.path.join("archive\Test_dir",folder)
    shutil.copy2(image_path, folder_path)