import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures


def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]
# make polynomial features
poly = PolynomialFeatures(degree=2)
xdegree2 = poly.fit_transform(x)

poly = PolynomialFeatures(degree=6)
xdegree6 = poly.fit_transform(x)

poly = PolynomialFeatures(degree=15)
xdegree15 = poly.fit_transform(x)

np.random.seed(12)
indeksi = np.random.permutation(len(xdegree2))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xdegree2)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xdegree2)))+1:len(xdegree2)]

xtrain2 = xdegree2[indeksi_train,]
xtrain6 = xdegree6[indeksi_train,]
xtrain15 = xdegree15[indeksi_train,]

ytrain = y_measured[indeksi_train]

xtest2 = xdegree2[indeksi_test,]
xtest6 = xdegree6[indeksi_test,]
xtest15 = xdegree15[indeksi_test,]

ytest = y_measured[indeksi_test]

linearModel2 = lm.LinearRegression()
linearModel6 = lm.LinearRegression()
linearModel15 = lm.LinearRegression()

linearModel2.fit(xtrain2,ytrain)
linearModel6.fit(xtrain6,ytrain)
linearModel15.fit(xtrain15,ytrain)

ytest_p2 = linearModel2.predict(xtest2)
ytest_p6 = linearModel6.predict(xtest6)
ytest_p15 = linearModel15.predict(xtest15)

ytrain_p2 = linearModel2.predict(xtrain2)
ytrain_p6 = linearModel6.predict(xtrain6)
ytrain_p15 = linearModel15.predict(xtrain15)

MSE_test=[]
MSE_train=[]

MSE_test.append(mean_squared_error(ytest, ytest_p2))
MSE_train.append(mean_squared_error(ytrain, ytrain_p2))

MSE_test.append(mean_squared_error(ytest, ytest_p6))
MSE_train.append(mean_squared_error(ytrain, ytrain_p6))

MSE_test.append(mean_squared_error(ytest, ytest_p15))
MSE_train.append(mean_squared_error(ytrain, ytrain_p15))


print("\nsrednje kvadratne pogreske za model 2. stupnja:")
print("test:", MSE_test[0])
print("train:", MSE_train[0])

print("\nsrednje kvadratne pogreske za model 6. stupnja:")
print("test:", MSE_test[1])
print("train:", MSE_train[1])

print("\nsrednje kvadratne pogreske za model 15. stupnja:")
print("test:", MSE_test[2])
print("train:", MSE_train[2])

#pozadinska funkcija vs modeli
plt.plot(x,y_true,label='f')
plt.plot(x, linearModel2.predict(xdegree2),'r-',label='model 2')
plt.plot(x, linearModel6.predict(xdegree6),'g-',label='model 6')
plt.plot(x, linearModel15.predict(xdegree15),'m-',label='model 15')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
plt.show()
#razlika je sto model vise nije linearan i pocinje pratiti krivulje modela